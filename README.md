# toggle3buttonmouse
Welcome to the repo Toggle 3 Button Mouse preference of Blender

Toggle 3 Button Mouse is an addon i built for artist beginners using graphic tablets and blender.

This addon is designed to facilitate the experience of beginner artists using graphics tablets in Blender. Addresses common difficulties when navigating 3D space without the use of the middle mouse, providing intuitive options for panning, zooming, and rotating the camera.

Tested and compatible with Blender 3.3.1, this plugin seeks to make creating in Blender more accessible and enjoyable for those who prefer to work with graphics tablets.

## Why this addon?
Some option are available when Emulate 3 Button Mouse settings are enabled while another are disabled and viceversa. For the momment theres no shortcut for enabled and disabled faster.

## How install it?
![addon_preview1](addon_preview_1.png)
![addon_preview1](addon_preview_2.png)

## How use it?

- For make a toggle faster you can:
With space bar use the search feature of Blender and write the addon
- Press Ctrl + Shift + Alt + Numpad 3
  
You will see a message to make you easy to know the state of the preference.    
