bl_info = {
    "name": "Toggle 3 Button Mouse",
    "blender": (3, 3, 1),
    "category": "3D View",
    "location": "View3D > Add > Mesh",
    "author": "Christopher Duran",
    "description": "Enable or Disable Emulate 3 Button Mouse Settings.",
    "version": (1, 0),
    "doc_url": "https://www.patreon.com/posts/blender-addon-3-95717422?utm_medium=clipboard_copy&utm_source=copyLink&utm_campaign=postshare_creator&utm_content=join_link",
    "tracker_url": "https://github.com/chriisduran/toggle3buttonmouse/issues",
    "support": "COMMUNITY",
}

import bpy
from bpy.types import AddonPreferences

class Toggle3ButtonMouseOperator(bpy.types.Operator):
    bl_idname = "wm.toggle_3_button_mouse"
    bl_label = "Toggle 3 Button Mouse"
    
    def execute(self, context):
        preferences = bpy.context.preferences.inputs
        preferences.use_mouse_emulate_3_button = not preferences.use_mouse_emulate_3_button
        self.report({'INFO'}, f"Addon {'Addon Enabled' if preferences.use_mouse_emulate_3_button else 'Addon Disabled'}")
        return {'FINISHED'}

def menu_func(self, context):
    self.layout.operator(Toggle3ButtonMouseOperator.bl_idname)

addon_keymaps = []

def register():
    bpy.utils.register_class(Toggle3ButtonMouseOperator)
    bpy.types.VIEW3D_MT_mesh_add.append(menu_func)

    # Combinación de teclas para activar el addon
    wm = bpy.context.window_manager
    kc = wm.keyconfigs.addon
    km = kc.keymaps.new(name='3D View', space_type='VIEW_3D')
    kmi = km.keymap_items.new(Toggle3ButtonMouseOperator.bl_idname, 'NUMPAD_3', 'PRESS', ctrl=True, shift=True, alt=True)

    addon_keymaps.append((km, kmi))

def unregister():
    bpy.utils.unregister_class(Toggle3ButtonMouseOperator)
    bpy.types.VIEW3D_MT_mesh_add.remove(menu_func)

    # Eliminar la combinación de teclas al desregistrar el addon
    wm = bpy.context.window_manager
    for km, kmi in addon_keymaps:
        km.keymap_items.remove(kmi)
    addon_keymaps.clear()

if __name__ == "__main__":
    register()
